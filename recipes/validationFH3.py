# coding: utf-8

from collections import defaultdict

import awkward as ak
import numpy as np
import hist
from hist import Hist
import coffea.processor as processor

import processor.util as util
from processor.sf import *
import tasks.corrections.processors as corr_proc

from utils.coffea import Histogramer, PackedSelection, Weights, hist_accumulator

from coffea.processor.accumulator import (
    AccumulatorABC,
    column_accumulator,
    defaultdict_accumulator,
    dict_accumulator,
    set_accumulator,
)


class BaseSelection:
    def get_pu_key(self, events):
        ds = self.get_dataset(events)
        if ds.is_data:
            return "data"
        else:
            return ds.name

    @property
    def trigger(self):
        return {
            2016: {
                self.config.get_channel("had"): {
                    "PFHT200": all,
                },
            },
            2017: {
                self.config.get_channel("had"): {
                    "PFHT180": all,
                },
            },
            2018: {
                self.config.get_channel("had"): {
                    "PFHT180": all,
                },
            },
        }[int(self.year)]

    debug_dataset = "TTToHadronic"

    def select(self, events):

        output = self.accumulator.identity()

        dataset_key = tuple(events.metadata["dataset"])
        dataset_inst = self.get_dataset(events)

        n_events = len(events)
        output["n_events"][dataset_key] = n_events

        for unc, shift, met, jets in self.jec_loop(events, jes_set=self.jes_set):  # , fatjets

            weights = Weights(n_events, dtype=np.float32, storeIndividual=True)  # or True

            # load objects
            ht = util.get_ht(jets)
            # trigger = util.Trigger(events.HLT, self.trigger, self.config, dataset_inst)

            # ht_cut = ht >= 250
            # # configure trigger:
            # trigger_had = util.reduce_and(
            #     trigger.get("had"),
            #     ht_cut,
            # )

            # configure channel
            # ch_had = util.reduce_and(trigger_had)

            # select good jets
            # jet_eta_cut = {2016: 2.4, 2017: 2.5, 2018: 2.5}[int(self.year)]
            jet_selection = (
                (jets.isTight)
                # & ((jets.puId >= 4) | (jets.pt >= 50.0))
                & (jets.pt > 30.0)
                & (abs(jets.eta) < 2.5)
            )

            good_jets = jets[jet_selection]
            clean_good_jets = good_jets

            # met filter
            if dataset_inst.is_data:
                met_filter = self.config.aux["met_filters"]["data"]
            else:
                met_filter = self.config.aux["met_filters"]["mc"]

            met_filter_mask = util.nano_mask_and(events.Flag, met_filter)

            jet_veto_mask = JetVetoMap(self.corrections)(
                clean_good_jets, weights, self.config.campaign.aux["veto_map_version"]
            )
            jet_veto_map = ak.all(jet_veto_mask == 0, axis=-1)

            # select jets
            n_jets = ak.num(good_jets)
            ge_two_jets = ak.num(good_jets) >= 2
            ge_six_jets = ak.num(good_jets) >= 6

            # min delta R between jets
            min_dr_jets = util.min_dr_part1_part2(good_jets, good_jets)
            good_jets["dr"] = min_dr_jets

            selection = PackedSelection()

            selection.add("met_filter", met_filter_mask)
            selection.add("ge_two_jets", ge_two_jets)
            selection.add("ge_six_jets", ge_six_jets)
            selection.add("jet_veto_map", jet_veto_map)
            # selection.add("ch_had", ch_had)

            # weights after object selection
            if dataset_inst.is_data:
                output["sum_gen_weights"][dataset_key] = 0.0

            if dataset_inst.is_mc:
                badgw = np.abs(events.Generator.weight) > 1e10
                gw = np.where(~badgw, events.Generator.weight, 0.0)
                output["sum_gen_weights"][dataset_key] = ak.sum(gw)
                weights.add("gen_weight", gw)

                if "btag" in self.corrections:
                    for b_tagger in self.config.aux["working_points"].keys():
                        if b_tagger == "deepjet":
                            discriminator = "btagDeepFlavB"
                        elif b_tagger == "PNet":
                            discriminator = "btagPNetB"
                        elif b_tagger == "ParT":
                            discriminator = "btagRobustParTAK4B"
                        else:
                            raise ValueError(f"Unknown b-tagger {b_tagger}")
                        # AK4 Jet reshape SF
                        corr = self.corrections["btag"][b_tagger].get("reshape")
                        assert shift is None, "No (JES) shifts needed for validation!"
                        shifts = list(self.config.aux.get("hf_and_lf_shifts", [])) + list(
                            self.config.aux.get("c_shifts", [])
                        )  # no shifts -> speedup
                        c = "central"
                        corr = corr.reduce(shifts=shifts)

                        sfs = ak.prod(
                            corr.eval_awk1(obj=good_jets[:, 0:1], discr=discriminator),
                            axis=-1,
                        )
                        print(sfs[c])
                        sf0 = sfs[c]
                        weights.add(
                            f"{b_tagger}_btagsf",
                            sf0,
                        )
                        for btag_shift in shifts:
                            weights.add(
                                f"{btag_shift}_{b_tagger}_btagsf",
                                ak.ones_like(sf0),
                                weightUp=sfs[f"up_{btag_shift}"] / sf0,
                                weightDown=sfs[f"down_{btag_shift}"] / sf0,
                            )
                        del sf0, sfs

                weights.add(
                    "pileup",
                    **self.corrections["pileup"](
                        pu_key=self.get_pu_key(events), nTrueInt=events.Pileup.nTrueInt
                    ),
                )

                flavour = abs(
                    ak.fill_none(
                        ak.firsts(clean_good_jets[:, 0:1].hadronFlavour),
                        -1,
                    )
                )
                light_mask = util.reduce_or(flavour < 4, flavour > 5)
                flav = ak.where(light_mask, 0, flavour)
                jet0_pt = ak.to_numpy(
                    ak.fill_none(ak.pad_none(clean_good_jets.pt, 1, clip=True)[:, 0], -999)
                )
                # print(jet0_pt)
                # l_flavour = (flavour_probe_jet < 4) | (flavour_probe_jet > 5)

            categories = dict()
            common_weights = [key for key in weights.weightStatistics.keys() if not "btag" in key]
            sfnorm_weights = dict()
            included_weights = dict()
            for b_tagger in self.config.aux["working_points"].keys():
                included_weights[f"btag_{b_tagger}"] = common_weights + [f"{b_tagger}_btagsf"]
                for ch in ["ch_had"]:
                    categories[f"{ch}_{b_tagger}"] = [
                        # f"{ch}",
                        "ge_two_jets",
                        "jet_veto_map",
                        "met_filter",
                    ]

            yield locals()


class Processor(BaseSelection, Histogramer):
    jes_shifts = False
    memory = "2600MiB" if jes_shifts else "2000MiB"

    def __init__(self, task):
        super().__init__(task)
        self._accumulator["histograms"] = dict_accumulator(
            {
                variable.name: hist_accumulator(
                    Hist(
                        self.dataset_axis,
                        self.category_axis,
                        self.syst_axis,
                        hist.axis.Variable(
                            variable.binning,
                            name=variable.name,
                            label=variable.x_title,
                        ),
                        hist.axis.Integer(0, 6, name="flavour", underflow=False, overflow=False),
                        hist.axis.Regular(
                            2,  # 48,
                            30,
                            500,
                            name="pt",
                            label="jet0_pt",
                        ),
                        # metadata={
                        #     "name": variable.name,
                        #     "x_title": variable.x_title,
                        #     "y_title": variable.y_title,
                        # },
                        storage=hist.storage.Weight(),
                    )
                )
                for variable in self.config.variables
            }
        )

    pass


class PUCount(BaseSelection, corr_proc.PUCount):
    memory = "1000MiB"
    pass
