# coding: utf-8

from collections import defaultdict

import awkward as ak
import numpy as np
import coffea.processor as processor

import processor.util as util
import tasks.corrections.processors as corr_proc

from utils.coffea import Histogramer, PackedSelection, Weights


class BaseSelection:
    def get_pu_key(self, events):
        ds = self.get_dataset(events)
        if ds.is_data:
            return "data"
        else:
            return ds.name

    @property
    def trigger(self):
        return {
            2016: {
                self.config.get_channel("ee"): {
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ": all,
                },
                self.config.get_channel("e"): {
                    "Ele27_WPTight_Gsf": all,
                },
                self.config.get_channel("mumu"): {
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL": all,  # Prescaled in run H by average factor of 26.5
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ": all,
                    "Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL": all,  # Prescaled in run H by average factor of 26.5
                    "Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL_DZ": all,
                },
                self.config.get_channel("mu"): {
                    "IsoMu24": all,
                    "IsoTkMu24": all,
                },
                self.config.get_channel("emu"): {
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL": all,  # Missing in run H
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,  # Introduced mid run F
                    "Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL": all,  # Missing in run H
                    "Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL_DZ": "H",  # Only in run H
                },
            },
            2017: {
                self.config.get_channel("ee"): {
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL": all,
                },
                self.config.get_channel("e"): {
                    "Ele35_WPTight_Gsf": all,
                    "Ele32_WPTight_Gsf": all,  # Missing in runs B and C
                },
                self.config.get_channel("emu"): {
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL": all,  # Missing in run B
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ": all,
                },
                self.config.get_channel("mumu"): {
                    # "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ": "B",???
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8": all,
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8": all,  # Missing in run B
                },
                self.config.get_channel("mu"): {
                    "IsoMu24": all,  # Prescaled at high lumi
                    "IsoMu27": all,
                },
            },
            2018: {
                self.config.get_channel("ee"): {
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL": all,
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ": all,
                },
                self.config.get_channel("e"): {
                    "Ele35_WPTight_Gsf": all,
                    "Ele28_eta2p1_WPTight_Gsf_HT150": all,  # Missing in runs B and C
                },
                self.config.get_channel("emu"): {
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL": all,
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ": all,
                },
                self.config.get_channel("mumu"): {
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8": all,
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8": all,
                },
                self.config.get_channel("mu"): {
                    "IsoMu24": all,
                },
            },
        }[int(self.year)]

    debug_dataset = "DYJetsToLL_M-10to50"

    def select(self, events):

        output = self.accumulator.identity()

        dataset_key = tuple(events.metadata["dataset"])
        dataset_inst = self.get_dataset(events)

        n_events = len(events)
        output["n_events"][dataset_key] = n_events

        # load objects
        muons = events.Muon
        electrons = events.Electron

        muon_selection = (
            ak.values_astype(muons.tightId, bool)
            & (muons.pt > 15.0)
            & (abs(muons.eta) <= 2.4)
            & (abs(muons.dxy) <= 0.05)
            & (abs(muons.dz) <= 0.1)
            & (muons.pfRelIso04_all <= 0.15)
            & (muons.sip3d <= 8.0)
        )

        ele_selection = (
            ak.values_astype(electrons.mvaFall17V2Iso_WP90, bool)
            & (electrons.pt > 15.0)
            & (abs(electrons.eta) <= 2.4)
            & (abs(electrons.dxy) <= 0.05)
            & (abs(electrons.dz) <= 0.1)
            & (electrons.sip3d <= 8.0)
            & (electrons.lostHits <= 1)
        )

        good_electrons = electrons[ele_selection]
        good_muons = muons[muon_selection]

        good_leptons = ak.with_name(
            ak.concatenate([good_muons, good_electrons], axis=1),
            "PtEtaPhiMCandidate",
        )

        # lepton cuts
        lep_tpls = ak.combinations(
            good_leptons[..., :2],
            n=2,
            replacement=False,
            axis=-1,
            fields=["l0", "l1"],
        )

        dilep = ak.firsts(lep_tpls.l0 + lep_tpls.l1)
        dilep_mass = ak.fill_none(dilep.mass, np.nan)
        zll_mass_min = dilep_mass >= 12.0
        zll_mass_cut = abs(dilep_mass - 91.1876) <= 10.0
        zll_mass_veto = abs(dilep_mass - 91.1876) >= 10.0
        z_pt_cut = ak.fill_none(dilep.pt, np.nan) > 10.0
        ll_opp_charge = ak.fill_none(dilep.charge, np.nan) == 0

        dr_ll = util.min_dr(good_leptons[..., :2])
        dr_ll_cut = dr_ll > 0.2

        # leading lepton pt cuts
        leading_muon_pt = ak.fill_none(ak.firsts(good_muons.pt), np.nan)
        leading_electron_pt = ak.fill_none(ak.firsts(good_electrons.pt), np.nan)
        leading_muon_pt_cut = leading_muon_pt > 25.0
        leading_electron_pt_cut = leading_electron_pt > 25.0
        single_muon_pt_cut = leading_muon_pt > 30.0
        single_electron_pt_cut = leading_electron_pt > 37.0

        trigger = util.Trigger(events.HLT, self.trigger, self.config, dataset_inst)

        # configure trigger:
        trigger_ee = util.reduce_and(
            trigger.get("ee"),
            leading_electron_pt_cut,
        )
        trigger_mumu = util.reduce_and(
            trigger.get("mumu"),
            leading_muon_pt_cut,
        )
        trigger_emu = util.reduce_and(
            trigger.get("emu"),
            util.reduce_or(leading_electron_pt_cut, leading_muon_pt_cut),
        )
        trigger_e = util.reduce_and(
            trigger.get("e"),
            single_electron_pt_cut,
        )
        trigger_mu = util.reduce_and(
            trigger.get("mu"),
            single_muon_pt_cut,
        )
        # configure channel
        ch_mumu = util.reduce_and(
            trigger_mumu,
            ak.num(good_muons) == 2,
            ak.num(good_electrons) == 0,
            zll_mass_min,
            ll_opp_charge,
            dr_ll_cut,
        )
        ch_ee = util.reduce_and(
            trigger_ee,
            ak.num(good_electrons) == 2,
            ak.num(good_muons) == 0,
            zll_mass_min,
            ll_opp_charge,
            dr_ll_cut,
        )
        ch_emu = util.reduce_and(
            trigger_emu,
            ak.num(good_electrons) == 1,
            ak.num(good_muons) == 1,
            zll_mass_min,
            ll_opp_charge,
            dr_ll_cut,
        )
        ch_mu = util.reduce_and(
            trigger_mu,
            ak.num(good_muons) == 1,
            ak.num(good_electrons) == 0,
        )
        ch_e = util.reduce_and(
            trigger_e,
            ak.num(good_electrons) == 1,
            ak.num(good_muons) == 0,
        )
        ch_l = util.reduce_or(ch_e, ch_mu)
        ch_ll = util.reduce_or(ch_ee, ch_emu, ch_mumu)

        for unc, shift, met, jets, fatjets in self.jec_loop(events, jes_set=self.jes_set):

            weights = Weights(n_events, dtype=np.float32, storeIndividual=True)

            # select good jets
            jet_eta_cut = {2016: 2.4, 2017: 2.5, 2018: 2.5}[int(self.year)]
            jet_selection = (
                (jets.isTight)
                & ((jets.puId >= 4) | (jets.pt >= 50.0))
                & (jets.pt > 20.0)
                & (abs(jets.eta) < jet_eta_cut)
            )

            good_jets = jets[jet_selection]

            # now clean all jets
            clean_good_jets = good_jets[
                util.nano_object_overlap(good_jets, good_leptons[:, :2], dr=0.4)
            ]

            # met filter
            if dataset_inst.is_data:
                met_filter = self.config.aux["met_filters"]["data"]
            else:
                met_filter = self.config.aux["met_filters"]["mc"]

            met_filter_mask = util.nano_mask_and(events.Flag, met_filter)

            # select jets
            n_jets = ak.num(clean_good_jets)
            ge_two_jets = ak.num(clean_good_jets) >= 2
            ge_four_jets = ak.num(clean_good_jets) >= 4

            met_pt_cut = met.pt > 30

            lepton_region_cut_HF = util.reduce_and(met_pt_cut, zll_mass_veto)
            lepton_region_cut_LF = util.reduce_and(
                ~met_pt_cut,
                zll_mass_cut,
                z_pt_cut,
            )
            # min delta R between jets
            min_dr_jets = util.min_dr_part1_part2(clean_good_jets, clean_good_jets)
            clean_good_jets["dr"] = min_dr_jets

            selection = PackedSelection()
            selection.add("met_filter", met_filter_mask)
            selection.add("ge_two_jets", ge_two_jets)
            selection.add("ge_four_jets", ge_four_jets)

            selection.add("lepton_region_cut_HF", lepton_region_cut_HF)
            selection.add("lepton_region_cut_LF", lepton_region_cut_LF)
            # lepton channel
            selection.add("ch_mumu", ch_mumu)
            selection.add("ch_ee", ch_ee)
            selection.add("ch_emu", ch_emu)
            selection.add("ch_mu", ch_mu)
            selection.add("ch_e", ch_e)
            selection.add("ch_l", ch_l)
            selection.add("ch_ll", ch_ll)

            # btag cuts
            for b_tagger in self.config.aux["working_points"].keys():
                if b_tagger == "deepcsv":
                    b_score = clean_good_jets.btagDeepB
                elif b_tagger == "deepjet":
                    b_score = clean_good_jets.btagDeepFlavB
                passbtag = b_score >= self.config.aux["working_points"][b_tagger]["medium"]
                zero_bjets = ak.num(clean_good_jets[passbtag]) == 0
                ge_one_bjets = ak.num(clean_good_jets[passbtag]) >= 1
                ge_two_bjets = ak.num(clean_good_jets[passbtag]) >= 2
                selection.add(f"zero_bjets_{b_tagger}", zero_bjets)  # 2L LF validation
                selection.add(f"ge_one_bjets_{b_tagger}", ge_one_bjets)  # 2L HF validation
                selection.add(f"ge_two_bjets_{b_tagger}", ge_two_bjets)  # 1L HF validation

            # weights after object selection
            if dataset_inst.is_data:
                output["sum_gen_weights"][dataset_key] = 0.0
                selection.add("lumimask", self.corrections["lumimask"](events))

            if dataset_inst.is_mc:
                selection.add("lumimask", np.ones(n_events, dtype=bool))

                badgw = np.abs(events.Generator.weight) > 1e10
                gw = np.where(~badgw, events.Generator.weight, 0.0)
                output["sum_gen_weights"][dataset_key] = ak.sum(gw)
                weights.add("gen_weight", gw)

                if "btag" in self.corrections:
                    for b_tagger in self.config.aux["working_points"].keys():
                        if b_tagger == "deepcsv":
                            discriminator = "btagDeepB"
                        elif b_tagger == "deepjet":
                            discriminator = "btagDeepFlavB"
                        else:
                            raise ValueError(f"Unknown b-tagger {b_tagger}")
                        # AK4 Jet reshape SF
                        corr = self.corrections["btag"][b_tagger].get("reshape")
                        assert shift is None, "No (JES) shifts needed for validation!"
                        shifts = list(self.config.aux.get("hf_and_lf_shifts", []))
                        c = "central"
                        corr = corr.reduce(shifts=shifts)

                        sfs = ak.prod(
                            corr.eval_awk1(obj=clean_good_jets, discr=discriminator),
                            axis=-1,
                        )
                        sf0 = sfs[c]
                        weights.add(
                            f"{b_tagger}_btagsf",
                            sf0,
                        )
                        for btag_shift in shifts:
                            weights.add(
                                f"{btag_shift}_{b_tagger}_btagsf",
                                ak.ones_like(sf0),
                                weightUp=sfs[f"up_{btag_shift}"] / sf0,
                                weightDown=sfs[f"down_{btag_shift}"] / sf0,
                            )
                        del sf0, sfs

                weights.add(
                    "pileup",
                    **self.corrections["pileup"](
                        pu_key=self.get_pu_key(events), nTrueInt=events.Pileup.nTrueInt
                    ),
                )

            # build categories
            common = ["met_filter", "lumimask"]

            categories = dict()
            common_weights = [key for key in weights.weightStatistics.keys() if not "btag" in key]
            sfnorm_weights = dict()
            included_weights = dict()
            for b_tagger in self.config.aux["working_points"].keys():
                # 1 lepton cats have no lepton_region_cut (Z related cuts)
                included_weights[f"btag_{b_tagger}"] = common_weights + [f"{b_tagger}_btagsf"]
                for ch in ["ch_e", "ch_mu"]:
                    categories[f"{ch}_2b_{b_tagger}"] = common + [
                        f"{ch}",
                        "ge_two_jets",
                        f"ge_two_bjets_{b_tagger}",
                    ]
                    categories[f"{ch}_4j2b_{b_tagger}"] = common + [
                        f"{ch}",
                        "ge_four_jets",
                        f"ge_two_bjets_{b_tagger}",
                    ]
                    categories[f"{ch}_{b_tagger}"] = common + [
                        f"{ch}",
                        "ge_two_jets",
                    ]
                for ch in ["ch_ee", "ch_mumu", "ch_emu"]:
                    categories[f"{ch}_HF_{b_tagger}"] = common + [
                        f"{ch}",
                        "ge_two_jets",
                        f"ge_one_bjets_{b_tagger}",
                        # "lepton_region_cut_HF",
                    ]
                    categories[f"{ch}_LF_{b_tagger}"] = common + [
                        f"{ch}",
                        "ge_two_jets",
                        f"zero_bjets_{b_tagger}",
                        # "lepton_region_cut_LF",
                    ]
                    categories[f"{ch}_{b_tagger}"] = common + [
                        f"{ch}",
                        "ge_two_jets",
                    ]

            yield locals()


class Processor(BaseSelection, Histogramer):
    jes_shifts = False
    memory = "2600MiB" if jes_shifts else "2000MiB"
    pass


class PUCount(BaseSelection, corr_proc.PUCount):
    memory = "1000MiB"
    pass
