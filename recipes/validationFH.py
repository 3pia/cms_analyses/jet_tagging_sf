# coding: utf-8

from collections import defaultdict

import awkward as ak
import numpy as np
import coffea.processor as processor

import processor.util as util
import tasks.corrections.processors as corr_proc

from utils.coffea import Histogramer, PackedSelection, Weights


class BaseSelection:
    def get_pu_key(self, events):
        ds = self.get_dataset(events)
        if ds.is_data:
            return "data"
        else:
            return ds.name

    @property
    def trigger(self):
        return {
            2016: {
                self.config.get_channel("had"): {
                    "PFHT200": all,
                },
            },
            2017: {
                self.config.get_channel("had"): {
                    "PFHT180": all,
                },
            },
            2018: {
                self.config.get_channel("had"): {
                    "PFHT180": all,
                },
            },
        }[int(self.year)]

    debug_dataset = "ZZ"

    def select(self, events):

        output = self.accumulator.identity()

        dataset_key = tuple(events.metadata["dataset"])
        dataset_inst = self.get_dataset(events)

        n_events = len(events)
        output["n_events"][dataset_key] = n_events

        for unc, shift, met, jets, fatjets in self.jec_loop(events, jes_set=self.jes_set):

            weights = Weights(n_events, dtype=np.float32, storeIndividual=True)  # or True

            # load objects
            ht = util.get_ht(jets)
            trigger = util.Trigger(events.HLT, self.trigger, self.config, dataset_inst)

            ht_cut = ht >= 250
            # configure trigger:
            trigger_had = util.reduce_and(
                trigger.get("had"),
                ht_cut,
            )

            # configure channel
            ch_had = util.reduce_and(trigger_had)

            # select good jets
            jet_eta_cut = {2016: 2.4, 2017: 2.5, 2018: 2.5}[int(self.year)]
            jet_selection = (
                (jets.isTight)
                & ((jets.puId >= 4) | (jets.pt >= 50.0))
                & (jets.pt > 20.0)
                & (abs(jets.eta) < jet_eta_cut)
            )

            good_jets = jets[jet_selection]
            clean_good_jets = good_jets

            # met filter
            if dataset_inst.is_data:
                met_filter = self.config.aux["met_filters"]["data"]
            else:
                met_filter = self.config.aux["met_filters"]["mc"]

            met_filter_mask = util.nano_mask_and(events.Flag, met_filter)

            # select jets
            n_jets = ak.num(good_jets)
            ge_six_jets = ak.num(good_jets) >= 6

            # min delta R between jets
            min_dr_jets = util.min_dr_part1_part2(good_jets, good_jets)
            good_jets["dr"] = min_dr_jets

            selection = PackedSelection()

            selection.add("ge_six_jets", ge_six_jets)
            selection.add("ch_had", ch_had)

            # weights after object selection
            if dataset_inst.is_data:
                output["sum_gen_weights"][dataset_key] = 0.0

            if dataset_inst.is_mc:
                badgw = np.abs(events.Generator.weight) > 1e10
                gw = np.where(~badgw, events.Generator.weight, 0.0)
                output["sum_gen_weights"][dataset_key] = ak.sum(gw)
                weights.add("gen_weight", gw)

                if "btag" in self.corrections:
                    for b_tagger in self.config.aux["working_points"].keys():
                        if b_tagger == "deepcsv":
                            discriminator = "btagDeepB"
                        elif b_tagger == "deepjet":
                            discriminator = "btagDeepFlavB"
                        else:
                            raise ValueError(f"Unknown b-tagger {b_tagger}")
                        # AK4 Jet reshape SF
                        corr = self.corrections["btag"][b_tagger].get("reshape")
                        assert shift is None, "No (JES) shifts needed for validation!"
                        shifts = []  # no shifts -> speedup
                        c = "central"
                        corr = corr.reduce(shifts=shifts)

                        sfs = ak.prod(
                            corr.eval_awk1(obj=good_jets, discr=discriminator),
                            axis=-1,
                        )
                        sf0 = sfs[c]
                        weights.add(
                            f"{b_tagger}_btagsf",
                            sf0,
                        )
                        del sf0, sfs

                weights.add(
                    "pileup",
                    **self.corrections["pileup"](
                        pu_key=self.get_pu_key(events), nTrueInt=events.Pileup.nTrueInt
                    ),
                )

            categories = dict()
            common_weights = [key for key in weights.weightStatistics.keys() if not "btag" in key]
            sfnorm_weights = dict()
            included_weights = dict()
            for b_tagger in self.config.aux["working_points"].keys():
                included_weights[f"btag_{b_tagger}"] = common_weights + [f"{b_tagger}_btagsf"]
                for ch in ["ch_had"]:
                    categories[f"{ch}_{b_tagger}"] = [
                        f"{ch}",
                        "ge_six_jets",
                    ]

            yield locals()


class Processor(BaseSelection, Histogramer):
    jes_shifts = False
    memory = "2600MiB" if jes_shifts else "2000MiB"
    pass


class PUCount(BaseSelection, corr_proc.PUCount):
    memory = "1000MiB"
    pass
