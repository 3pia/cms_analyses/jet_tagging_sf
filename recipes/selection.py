# coding: utf-8

from collections import defaultdict

import awkward as ak
import numpy as np
import coffea.processor as processor

import processor.util as util
import tasks.corrections.processors as corr_proc

from utils.coffea import Histogramer, PackedSelection, Weights


class BaseSelection:
    def get_pu_key(self, events):
        ds = self.get_dataset(events)
        if ds.is_data:
            return "data"
        else:
            return ds.name

    @property
    def trigger(self):
        return {
            2016: {
                self.config.get_channel("ee"): {
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ": all,
                },
                self.config.get_channel("e"): {
                    "Ele27_WPTight_Gsf": all,
                },
                self.config.get_channel("mumu"): {
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL": all,  # Prescaled in run H by average factor of 26.5
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ": all,
                    "Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL": all,  # Prescaled in run H by average factor of 26.5
                    "Mu17_TrkIsoVVL_TkMu8_TrkIsoVVL_DZ": all,
                },
                self.config.get_channel("mu"): {
                    "IsoMu24": all,
                    "IsoTkMu24": all,
                },
                self.config.get_channel("emu"): {
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL": all,  # Missing in run H
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,  # Introduced mid run F
                    "Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL": all,  # Missing in run H
                    "Mu23_TrkIsoVVL_Ele8_CaloIdL_TrackIdL_IsoVL_DZ": "H",  # Only in run H
                },
            },
            2017: {
                self.config.get_channel("ee"): {
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL": all,
                },
                self.config.get_channel("e"): {
                    "Ele35_WPTight_Gsf": all,
                    "Ele32_WPTight_Gsf": all,  # Missing in runs B and C
                },
                self.config.get_channel("emu"): {
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL": all,  # Missing in run B
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ": all,
                },
                self.config.get_channel("mumu"): {
                    # "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ": "B",???
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8": all,
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8": all,  # Missing in run B
                },
                self.config.get_channel("mu"): {
                    "IsoMu24": all,  # Prescaled at high lumi
                    "IsoMu27": all,
                },
            },
            2018: {
                self.config.get_channel("ee"): {
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL": all,
                    "Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ": all,
                },
                self.config.get_channel("e"): {
                    "Ele35_WPTight_Gsf": all,
                    "Ele28_eta2p1_WPTight_Gsf_HT150": all,  # Missing in runs B and C
                },
                self.config.get_channel("emu"): {
                    "Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu12_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ": all,
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL": all,
                    "Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ": all,
                },
                self.config.get_channel("mumu"): {
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8": all,
                    "Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8": all,
                },
                self.config.get_channel("mu"): {
                    "IsoMu24": all,
                },
            },
        }[int(self.year)]

    debug_dataset = "TTTo2L2Nu"  # "DYJetsToLL_M-10to50" # "ZZ"  #   # "WW"  #"data_F_mumu"
    # debug_uuids = {"03FF9FFB-1D9E-EB40-A0CA-D7ABA97565EA"}

    def select(self, events):

        output = self.accumulator.identity()

        dataset_key = tuple(events.metadata["dataset"])
        dataset_inst = self.get_dataset(events)

        n_events = len(events)
        output["n_events"][dataset_key] = n_events

        # load objects
        muons = events.Muon
        electrons = events.Electron

        muon_selection = (
            ak.values_astype(muons.tightId, bool)
            & (muons.pt > 15.0)
            & (abs(muons.eta) <= 2.4)
            & (abs(muons.dxy) <= 0.05)
            & (abs(muons.dz) <= 0.1)
            & (muons.pfRelIso04_all <= 0.15)
            & (muons.sip3d <= 8.0)
        )

        ele_selection = (
            ak.values_astype(electrons.mvaFall17V2Iso_WP90, bool)
            & (electrons.pt > 15.0)
            & (abs(electrons.eta) <= 2.4)
            & (abs(electrons.dxy) <= 0.05)
            & (abs(electrons.dz) <= 0.1)
            & (electrons.sip3d <= 8.0)
            & (electrons.lostHits <= 1)
        )

        good_electrons = electrons[ele_selection]
        good_muons = muons[muon_selection]

        good_leptons = ak.with_name(
            ak.concatenate([good_muons, good_electrons], axis=1),
            "PtEtaPhiMCandidate",
        )

        # lepton cuts
        lep_tpls = ak.combinations(
            good_leptons[..., :2],
            n=2,
            replacement=False,
            axis=-1,
            fields=["l0", "l1"],
        )

        dilep = ak.firsts(lep_tpls.l0 + lep_tpls.l1)
        dilep_mass = ak.fill_none(dilep.mass, np.nan)
        zll_mass_min = dilep_mass >= 12.0
        zll_mass_cut = abs(dilep_mass - 91.1876) <= 10.0
        zll_mass_veto = abs(dilep_mass - 91.1876) >= 10.0
        z_pt_cut = ak.fill_none(dilep.pt, np.nan) > 10.0
        ll_opp_charge = ak.fill_none(dilep.charge, np.nan) == 0

        dr_ll = util.min_dr(good_leptons[..., :2])
        dr_ll_cut = dr_ll > 0.2

        # leading lepton pt cuts
        leading_muon_pt = ak.fill_none(ak.firsts(good_muons.pt), np.nan)
        leading_electron_pt = ak.fill_none(ak.firsts(good_electrons.pt), np.nan)
        leading_muon_pt_cut = leading_muon_pt > 25.0
        leading_electron_pt_cut = leading_electron_pt > 25.0

        trigger = util.Trigger(events.HLT, self.trigger, self.config, dataset_inst)

        # configure trigger:
        trigger_ee = util.reduce_and(
            trigger.get("ee"),
            leading_electron_pt_cut,
        )
        trigger_mumu = util.reduce_and(
            trigger.get("mumu"),
            leading_muon_pt_cut,
        )
        trigger_emu = util.reduce_and(
            trigger.get("emu"),
            util.reduce_or(leading_electron_pt_cut, leading_muon_pt_cut),
        )

        # configure channel
        ch_mumu = util.reduce_and(
            trigger_mumu,
            ak.num(good_muons) == 2,
            ak.num(good_electrons) == 0,
            zll_mass_min,
            ll_opp_charge,
            dr_ll_cut,
        )
        ch_ee = util.reduce_and(
            trigger_ee,
            ak.num(good_electrons) == 2,
            ak.num(good_muons) == 0,
            zll_mass_min,
            ll_opp_charge,
            dr_ll_cut,
        )
        ch_emu = util.reduce_and(
            trigger_emu,
            ak.num(good_electrons) == 1,
            ak.num(good_muons) == 1,
            zll_mass_min,
            ll_opp_charge,
            dr_ll_cut,
        )

        for unc, shift, met, jets, fatjets in self.jec_loop(events, jes_set=self.jes_set):

            weights = Weights(n_events, dtype=np.float32, storeIndividual=True)

            # select good jets
            jet_eta_cut = {2016: 2.4, 2017: 2.5, 2018: 2.5}[int(self.year)]
            jet_selection = (
                (jets.isTight)
                & ((jets.puId >= 4) | (jets.pt >= 50.0))
                & (jets.pt > 20.0)
                & (abs(jets.eta) < jet_eta_cut)
            )

            good_jets = jets[jet_selection]

            # now clean all objects
            clean_good_jets = good_jets[
                util.nano_object_overlap(good_jets, good_leptons[:, :2], dr=0.4)
            ]

            # met filter
            if dataset_inst.is_data:
                met_filter = self.config.aux["met_filters"]["data"]
            else:
                met_filter = self.config.aux["met_filters"]["mc"]

            met_filter_mask = util.nano_mask_and(events.Flag, met_filter)

            # select two leptons
            # select 2 jets
            n_jets = ak.num(clean_good_jets)
            two_jets = n_jets == 2
            ge_two_jets = n_jets >= 2

            met_pt_cut = met.pt > 30
            mht_x = ak.sum(clean_good_jets.x, axis=-1) + ak.sum(good_leptons.x, axis=-1)
            mht_y = ak.sum(clean_good_jets.y, axis=-1) + ak.sum(good_leptons.y, axis=-1)
            mht = np.sqrt(mht_x ** 2 + mht_y ** 2)
            z_diamond = (
                (dilep_mass < (65.5 + 3 * mht / 8))
                | (dilep_mass > (108.0 - mht / 4))
                | (dilep_mass < (79.0 - 3 * mht / 4))
                | (dilep_mass > (99.0 + mht / 2))
            )

            lepton_region_cut_HF = util.reduce_and(met_pt_cut, zll_mass_veto)
            lepton_region_cut_LF = util.reduce_and(~met_pt_cut, zll_mass_cut, z_pt_cut, ~z_diamond)

            # min delta R between jets
            min_dr_jets = util.min_dr_part1_part2(clean_good_jets, clean_good_jets)
            clean_good_jets["dr"] = min_dr_jets

            selection = PackedSelection()
            selection.add("met_filter", met_filter_mask)
            selection.add("two_jets", two_jets)
            selection.add("ge_two_jets", ge_two_jets)

            selection.add("lepton_region_cut_HF", lepton_region_cut_HF)
            selection.add("lepton_region_cut_LF", lepton_region_cut_LF)
            # lepton channel
            selection.add("ch_mumu", ch_mumu)
            selection.add("ch_ee", ch_ee)
            selection.add("ch_emu", ch_emu)

            # weights after object selection
            if dataset_inst.is_data:
                output["sum_gen_weights"][dataset_key] = 0.0
                selection.add("lumimask", self.corrections["lumimask"](events))

            # print(self.corrections["lumimask"](events))
            # print(met_filter_mask)
            # print(
            #     np.all(
            #         util.reduce_and(self.corrections["lumimask"](events), met_filter_mask)
            #         == met_filter_mask
            #     )
            # )

            if dataset_inst.is_mc:
                selection.add("lumimask", np.ones(n_events, dtype=bool))

                badgw = np.abs(events.Generator.weight) > 1e10
                gw = np.where(~badgw, events.Generator.weight, 0.0)
                output["sum_gen_weights"][dataset_key] = ak.sum(gw)
                weights.add("gen_weight", gw)

                HF_clean_jets = clean_good_jets[clean_good_jets.hadronFlavour == 5]
                LF_clean_jets = clean_good_jets[
                    (clean_good_jets.hadronFlavour < 4) | (clean_good_jets.hadronFlavour > 5)
                ]
                if ("btag" in self.corrections) and (self.iteration < 3):
                    for region, sf_jets in zip(
                        ["HF", "LF"],
                        [HF_clean_jets, LF_clean_jets],
                    ):
                        for b_tagger in self.config.aux["working_points"].keys():
                            if b_tagger == "deepcsv":
                                discriminator = "btagDeepB"
                            elif b_tagger == "deepjet":
                                discriminator = "btagDeepFlavB"
                            else:
                                raise ValueError(f"Unknown b-tagger {b_tagger}")

                            corr = self.corrections["btag"][b_tagger].get("reshape")
                            if shift is None:
                                shifts = list(self.config.aux.get("hf_and_lf_shifts", []))
                                c = "central"
                                corr = corr.reduce(shifts=shifts)
                            else:
                                shifts = []
                                if unc in ("jer", "UnclustEn", "HemIssue"):
                                    c = "central"
                                else:
                                    btag_shift = shift
                                    _unc = unc.replace("Total", "")
                                    c = f"{btag_shift}_jes{_unc}"
                                assert c in corr.shifts, (c, corr.shifts)
                                corr = corr.reduce(shifts=[c], updown=False)
                            for probe_jet in [0, 1]:

                                sfs = ak.prod(
                                    corr.eval_awk1(
                                        obj=sf_jets[:, probe_jet : probe_jet + 1],
                                        discr=discriminator,
                                    ),
                                    axis=-1,
                                )
                                sf0 = sfs[c]

                                weights.add(
                                    f"btagWeight_{b_tagger}_probe_jet{probe_jet}_{region}",
                                    sf0,
                                )
                                for btag_shift in shifts:
                                    weights.add(
                                        f"btagWeight_{btag_shift}_{b_tagger}_probe_jet{probe_jet}_{region}",
                                        ak.ones_like(sf0),
                                        weightUp=sfs[f"up_{btag_shift}"] / sf0,
                                        weightDown=sfs[f"down_{btag_shift}"] / sf0,
                                    )
                                del sf0, sfs

                if self.iteration >= 3:
                    for b_tagger in self.config.aux["working_points"].keys():
                        if b_tagger == "deepcsv":
                            discriminator = "btagDeepB"
                        elif b_tagger == "deepjet":
                            discriminator = "btagDeepFlavB"
                        else:
                            raise ValueError(f"Unknown b-tagger {b_tagger}")
                        # AK4 Jet reshape SF
                        corr = self.corrections["btag"][b_tagger].get("reshape")
                        if shift is None:
                            shifts = list(self.config.aux.get("hf_and_lf_shifts", [])) + list(
                                self.config.aux.get("c_shifts", [])
                            )
                            c = "central"
                            corr = corr.reduce(shifts=shifts)
                        else:
                            shifts = []
                            if unc in ("jer", "UnclustEn", "HemIssue"):
                                c = "central"
                            else:
                                btag_shift = shift
                                _unc = unc.replace("Total", "")
                                c = f"{btag_shift}_jes{_unc}"
                            assert c in corr.shifts, (c, corr.shifts)
                            corr = corr.reduce(shifts=[c], updown=False)
                        for probe_jet in [0, 1]:
                            sfs = ak.prod(
                                corr.eval_awk1(
                                    obj=clean_good_jets[:, probe_jet : probe_jet + 1],
                                    discr=discriminator,
                                ),
                                axis=-1,
                            )
                            sf0 = sfs[c]
                            weights.add(
                                f"{b_tagger}_probe_jet{probe_jet}_btagsf",
                                sf0,
                            )
                            for btag_shift in shifts:
                                weights.add(
                                    f"{btag_shift}_{b_tagger}_probe_jet{probe_jet}_btagsf",
                                    ak.ones_like(sf0),
                                    weightUp=sfs[f"up_{btag_shift}"] / sf0,
                                    weightDown=sfs[f"down_{btag_shift}"] / sf0,
                                )
                            del sf0, sfs

                weights.add(
                    "pileup",
                    **self.corrections["pileup"](
                        pu_key=self.get_pu_key(events), nTrueInt=events.Pileup.nTrueInt
                    ),
                )

                # L1 ECAL prefiring
                if self.year in ("2016", "2017"):
                    weights.add(
                        "l1_ecal_prefiring",
                        events.L1PreFiringWeight.Nom,
                        # weightUp=events.L1PreFiringWeight.Up,
                        # weightDown=events.L1PreFiringWeight.Dn,
                    )
            for i_tag_jet, i_probe_jet in [(0, 1), (1, 0)]:
                for b_tagger in self.config.aux["working_points"].keys():
                    if b_tagger == "deepjet":
                        b_score_tag_jet = ak.fill_none(
                            ak.firsts(clean_good_jets[:, i_tag_jet : i_tag_jet + 1].btagDeepFlavB),
                            np.nan,
                        )
                    elif b_tagger == "deepcsv":
                        b_score_tag_jet = ak.fill_none(
                            ak.firsts(clean_good_jets[:, i_tag_jet : i_tag_jet + 1].btagDeepB),
                            np.nan,
                        )
                    else:
                        raise ValueError(f"Unknown b-tagger {b_tagger}")

                    tag_jet_cut = {
                        "HF": b_score_tag_jet
                        >= self.config.aux["working_points"][b_tagger]["medium"],
                        "LF": b_score_tag_jet
                        <= self.config.aux["working_points"][b_tagger]["loose"],
                    }
                    selection.add(
                        f"tag_jet_cut_{i_tag_jet}_{b_tagger}_HF",
                        ak.to_numpy(tag_jet_cut["HF"]),
                    )
                    selection.add(
                        f"tag_jet_cut_{i_tag_jet}_{b_tagger}_LF",
                        ak.to_numpy(tag_jet_cut["LF"]),
                    )
                # flavour_cut
                if dataset_inst.is_mc:
                    flavour_probe_jet = abs(
                        ak.fill_none(
                            ak.firsts(
                                clean_good_jets[:, i_probe_jet : i_probe_jet + 1].hadronFlavour
                            ),
                            0.0,
                        )
                    )
                    b_flavour = flavour_probe_jet == 5
                    c_flavour = flavour_probe_jet == 4
                    l_flavour = (flavour_probe_jet < 4) | (flavour_probe_jet > 5)
                    flavours = {"b": b_flavour, "c": c_flavour, "l": l_flavour}
                else:
                    incl_flavour = np.array([True] * n_events)
                    flavours = {"incl": incl_flavour}
                for flavour, flavour_cut in flavours.items():
                    selection.add(f"{flavour}_{i_probe_jet}", ak.to_numpy(flavour_cut))

                # pt eta cuts
                for region in ["HF", "LF"]:
                    pt_binning = self.config.aux["binning"][region]["pt"]
                    eta_binning = self.config.aux["binning"][region]["abs_eta"]
                    dr_binning = self.config.aux["binning"][region]["dr"]
                    pt_probe_jet = ak.fill_none(
                        ak.firsts(clean_good_jets[:, i_probe_jet : i_probe_jet + 1].pt),
                        np.nan,
                    )
                    abs_eta_probe_jet = abs(
                        ak.fill_none(
                            ak.firsts(clean_good_jets[:, i_probe_jet : i_probe_jet + 1].eta),
                            np.nan,
                        )
                    )
                    dr_probe_jet = ak.fill_none(
                        ak.firsts(min_dr_jets[:, i_probe_jet : i_probe_jet + 1]),
                        np.nan,
                    )
                    for pt_min, pt_max in zip(pt_binning[:-1], pt_binning[1:]):
                        pt_cut = (pt_min < pt_probe_jet) & (pt_max > pt_probe_jet)
                        selection.add(
                            f"pt_cut_{i_probe_jet}_{region}_pt{pt_min}to{pt_max}",
                            pt_cut,
                        )
                    for eta_min, eta_max in zip(eta_binning[:-1], eta_binning[1:]):
                        eta_cut = (eta_min < abs_eta_probe_jet) & (eta_max > abs_eta_probe_jet)
                        selection.add(
                            f"eta_cut_{i_probe_jet}_{region}_eta{eta_min}to{eta_max}",
                            eta_cut,
                        )
                    for dr_min, dr_max in zip(dr_binning[:-1], dr_binning[1:]):
                        dr_cut = (dr_min < dr_probe_jet) & (dr_max > dr_probe_jet)
                        selection.add(
                            f"dr_cut_{i_probe_jet}_{region}_dr{dr_min}to{dr_max}",
                            dr_cut,
                        )

            # build categories
            common = ["met_filter", "lumimask", "ge_two_jets"]
            common_scale = ["met_filter", "lumimask", "ge_two_jets"]
            categories = dict()
            common_weights = [key for key in weights.weightStatistics.keys() if not "btag" in key]
            sfnorm_weights = dict()
            included_weights = dict()
            included_shifts = dict()
            for b_tagger in self.config.aux["working_points"].keys():
                for i_tag_jet, i_probe_jet in [(0, 1), (1, 0)]:
                    included_weights[f"jet{i_probe_jet}_btag_{b_tagger}"] = {}
                    for region, opposite_region in zip(["HF", "LF"], ["LF", "HF"]):
                        # add weight only for the opposite flavour contribution
                        # region: common_weights + [f"{b_tagger}_{opposite_region}"]
                        included_weights[f"jet{i_probe_jet}_btag_{b_tagger}"][
                            region
                        ] = common_weights + [
                            f"{b_tagger}_probe_jet{i_probe_jet}_{opposite_region}"
                        ]
                        # for plotting and norm use full sf not just opposite region
                        if self.iteration >= 3:
                            included_weights[f"jet{i_probe_jet}_btag_{b_tagger}"][
                                region
                            ] = common_weights + [f"{b_tagger}_probe_jet{i_probe_jet}_btagsf"]
                        pt_binning = self.config.aux["binning"][region]["pt"]
                        eta_binning = self.config.aux["binning"][region]["abs_eta"]
                        dr_binning = self.config.aux["binning"][region]["dr"]
                        for pt_min, pt_max in zip(pt_binning[:-1], pt_binning[1:]):
                            for eta_min, eta_max in zip(eta_binning[:-1], eta_binning[1:]):
                                for dr_min, dr_max in zip(dr_binning[:-1], dr_binning[1:]):
                                    for flavour in flavours.keys():
                                        for ch in ["ch_ee", "ch_mumu", "ch_emu"]:
                                            if ch == "ch_emu" and region == "LF":
                                                continue
                                            categories[
                                                f"{region}_{ch}_{b_tagger}_probe_jet{i_probe_jet}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_dr{dr_min}to{dr_max}_{flavour}"
                                            ] = common + [
                                                f"{ch}",
                                                f"tag_jet_cut_{i_tag_jet}_{b_tagger}_{region}",
                                                f"pt_cut_{i_probe_jet}_{region}_pt{pt_min}to{pt_max}",
                                                f"eta_cut_{i_probe_jet}_{region}_eta{eta_min}to{eta_max}",
                                                f"dr_cut_{i_probe_jet}_{region}_dr{dr_min}to{dr_max}",
                                                f"{flavour}_{i_probe_jet}",
                                                f"lepton_region_cut_{region}",
                                            ]
                                            if self.iteration == 3:
                                                # remove any flavour related cuts for inclusive normalisation
                                                categories[
                                                    f"{region}_{ch}_{b_tagger}_probe_jet{i_probe_jet}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_dr{dr_min}to{dr_max}_{flavour}"
                                                ] = common + [
                                                    f"{ch}",
                                                    # f"tag_jet_cut_{i_tag_jet}_{b_tagger}_{region}",
                                                    f"pt_cut_{i_probe_jet}_{region}_pt{pt_min}to{pt_max}",
                                                    f"eta_cut_{i_probe_jet}_{region}_eta{eta_min}to{eta_max}",
                                                    f"dr_cut_{i_probe_jet}_{region}_dr{dr_min}to{dr_max}",
                                                    f"{flavour}_{i_probe_jet}",
                                                    # f"lepton_region_cut_{region}",
                                                ]
                                            included_shifts[
                                                f"{region}_{ch}_{b_tagger}_probe_jet{i_probe_jet}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_dr{dr_min}to{dr_max}_{flavour}"
                                            ] = f"{b_tagger}_probe_jet{i_probe_jet}_{opposite_region}"
                                            if self.iteration >= 3:
                                                included_shifts[
                                                    f"{region}_{ch}_{b_tagger}_probe_jet{i_probe_jet}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_dr{dr_min}to{dr_max}_{flavour}"
                                                ] = f"{b_tagger}_probe_jet{i_probe_jet}_btagsf"
                                            sfnorm_weights[
                                                f"{region}_{ch}_{b_tagger}_probe_jet{i_probe_jet}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_dr{dr_min}to{dr_max}_{flavour}"
                                            ] = common_weights + [
                                                f"{b_tagger}_probe_jet{i_probe_jet}_btagsf"
                                            ]
            for region in ["HF", "LF"]:
                for ch in ["ch_ee", "ch_mumu", "ch_emu"]:
                    category_name = f"{region}_{ch}"
                    categories[category_name] = common_scale + [
                        f"{ch}",
                        f"lepton_region_cut_{region}",
                    ]
                    included_shifts[f"{region}_{ch}"] = ""
            # fix jes naming
            if unc not in ["nominal", "UnclustEn", "jer", "HemIssue"]:
                unc = f"jes_{unc}_"
            yield locals()


class Processor(BaseSelection, Histogramer):
    jes_shifts = True
    memory = "2600MiB" if jes_shifts else "2000MiB"

    def __init__(self, task):
        super().__init__(task)
        # for plotting final result
        if task.iteration >= 4:
            self.jes_shifts = False
            self.memory = "2000MiB"


class PUCount(BaseSelection, corr_proc.PUCount):
    memory = "1000MiB"
