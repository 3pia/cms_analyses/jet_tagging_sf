from itertools import chain
import numpy as np

variables_dict = {
    # "njets": {
    #     "expression": "n_jets",
    #     "binning": list(np.linspace(0.5, 15.5, 15 + 1)),  # (15, 0.5, 15.5),
    #     "x_title": r"$N_{J}$",
    #     "aux": {"plot": True},
    # },
    "deepjet": {
        "expression": "1",
        "binning": list(np.linspace(0.0, 1.0, 10 + 1)),  # (10, 0.0, 1.0),
        "x_title": f"Jet btag value (deepJet)",
        "unit": "units",
        "aux": {"plot": True, "no_processing": True},
    },
    "PNet": {
        "expression": "1",
        "binning": list(np.linspace(0.0, 1.0, 10 + 1)),
        "x_title": f"Jet btag value (particleNet)",
        "unit": "units",
        "aux": {"plot": True, "no_processing": True},
    },
    "ParT": {
        "expression": "1",
        "binning": list(np.linspace(0.0, 1.0, 10 + 1)),
        "x_title": f"Jet btag value (robustParT)",
        "unit": "units",
        "aux": {"plot": True, "no_processing": True},
    },
    # "nPV": {
    #     "expression": "pv.npvsGood",
    #     "binning": list(np.linspace(0.0, 80.0, 80 + 1)),  # (15, 0.5, 15.5),
    #     "x_title": r"nPV (good)",
    #     "aux": {"plot": True, "validation_only": True},
    # },
    # "Rho_Carlo": {
    #     "expression": "rho.fixedGridRhoFastjetCentralCalo",
    #     "binning": list(np.linspace(0.0, 50.0, 50 + 1)),  # (15, 0.5, 15.5),
    #     "x_title": r"fixedGridRhoFastjetCentralCalo",
    #     "aux": {"plot": True},
    # },
    # "Rho_ChargedPileup": {
    #     "expression": "rho.fixedGridRhoFastjetCentralChargedPileUp",
    #     "binning": list(np.linspace(0.0, 50.0, 50 + 1)),  # (15, 0.5, 15.5),
    #     "x_title": r"fixedGridRhoFastjetCentralChargedPileUp",
    #     "aux": {"plot": True},
    # },
    # "deepcsv": {
    #     "expression": "1",
    #     "binning": list(np.linspace(0.0, 1.0, 10 + 1)),
    #     "x_title": f"Jet btag value",
    #     "aux": {"plot": True, "no_processing": True},
    # },
    # ak4 jets
    **dict(
        list(
            chain.from_iterable(
                [
                    [
                        # (
                        #     f"jet{i}_pt",
                        #     {
                        #         "expression": f"ak.to_numpy(ak.fill_none(ak.pad_none(clean_good_jets.pt,{i+1}, clip=True)[:, {i}],-999))",
                        #         "binning": list(np.linspace(0.0, 500.0, 25)),
                        #         "unit": "GeV",
                        #         "x_title": fr"Jet {i} $p_T$",
                        #         "aux": {"plot": True, "validation_only": True},
                        #     },
                        # ),
                        # (
                        #     f"jet{i}_eta",
                        #     {
                        #         "expression": f"ak.to_numpy(ak.fill_none(ak.pad_none(clean_good_jets.eta,{i+1}, clip=True)[:, {i}],-999))",
                        #         "binning": list(np.linspace(-2.5, 2.5, 25)),
                        #         "x_title": fr"Jet {i} $\eta$",
                        #         "aux": {"plot": True, "validation_only": True},
                        #     },
                        # ),
                        # (
                        #     f"jet{i}_phi",
                        #     {
                        #         "expression": f"ak.to_numpy(ak.fill_none(ak.pad_none(clean_good_jets.phi,{i+1}, clip=True)[:, {i}],-999))",
                        #         "binning": list(np.linspace(-np.pi, np.pi, 20)),
                        #         "x_title": fr"Jet {i} $\phi$",
                        #         "aux": {"plot": True, "validation_only": True},
                        #     },
                        # ),
                        # (
                        #     f"jet{i}_min_dR",
                        #     {
                        #         "expression": f"ak.to_numpy(ak.fill_none(ak.pad_none(min_dr_jets, {i + 1}, clip=True)[:, {i}], -999))",
                        #         "binning": list(np.linspace(0.0, 6.0, 15 + 1)),  # (12,0.0,6.0),
                        #         "x_title": fr"Jet {i}: $\Delta$R to closest Jet",
                        #         "aux": {"plot": True},
                        #     },
                        # ),
                        (
                            f"jet{i}_btag_deepjet",
                            {
                                "expression": f"ak.to_numpy(ak.fill_none(ak.pad_none(clean_good_jets.btagDeepFlavB, {i + 1}, clip=True)[:, {i}], -999))",
                                "binning": list(np.linspace(0.0, 1.0, 1000 + 1)),
                                # (100,0.0,1.0),  # old 200, new min bin width 0.02
                                "x_title": f"Jet {i} btag value",
                                "aux": {"plot": True},
                            },
                        ),
                        (
                            f"jet{i}_btag_PNet",
                            {
                                "expression": f"ak.to_numpy(ak.fill_none(ak.pad_none(clean_good_jets.btagPNetB, {i + 1}, clip=True)[:, {i}], -999))",
                                "binning": list(np.linspace(0.0, 1.0, 1000 + 1)),
                                # (100,0.0,1.0),  # old 200, new min bin width 0.02
                                "x_title": f"Jet {i} btag value",
                                "aux": {"plot": True},
                            },
                        ),
                        (
                            f"jet{i}_btag_ParT",
                            {
                                "expression": f"ak.to_numpy(ak.fill_none(ak.pad_none(clean_good_jets.btagRobustParTAK4B, {i + 1}, clip=True)[:, {i}], -999))",
                                "binning": list(np.linspace(0.0, 1.0, 1000 + 1)),
                                # (100,0.0,1.0),  # old 200, new min bin width 0.02
                                "x_title": f"Jet {i} btag value",
                                "aux": {"plot": True},
                            },
                        ),
                        # (
                        #     f"jet{i}_btag_deepcsv",
                        #     {
                        #         "expression": f"ak.to_numpy(ak.fill_none(ak.pad_none(clean_good_jets.btagDeepB, {i + 1}, clip=True)[:, {i}],-999))",
                        #         "binning": [-2.0] + list(np.linspace(0.0, 1.0, 100 + 1)),
                        #         # (300,-2.0,1.0),  # #old 600, new min bin width 0.02
                        #         "x_title": f"Jet {i} btag value",
                        #         "aux": {"plot": True},
                        #     },
                        # ),
                    ]
                    for i in range(0, 1 + 1)
                ]
            )
        )
    ),
}


def setup_variables(cfg):
    id = 0
    for name, attributes in variables_dict.items():
        cfg.add_variable(name=name, id=id, **attributes)
        id += 1
