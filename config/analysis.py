import numpy as np
import scinum as sn
import order as od
import six
import copy

#
# analysis and config
#
# import config.Run2_pp_13TeV_ULpreVFP_2016 as run_UL  # preVFP
import config.Run3_pp_136TeV_2022 as run_22
import config.Run3_pp_136TeV_EE_2022 as run_22EE

import config.Run3_pp_136TeV_2023 as run_23
import config.Run3_pp_136TeV_BPix_2023 as run_23BPix

# import config.Run3_pp_136TeV_2022 as run_UL
from config.util import PrepareConfig

get = od.Process.get_instance


def rgb(r, g, b):
    return (r, g, b)


# create the analysis
analysis = od.Analysis("jet_tagging_sf", 1)

# setup the config for campaign
config_22 = run_22.default_config(analysis=analysis)
config_22EE = run_22EE.default_config(analysis=analysis)

config_23 = run_23.default_config(analysis=analysis, context=run_23.campaign.name)
config_23BPix = run_23BPix.default_config(analysis=analysis, context=run_23BPix.campaign.name)

od.Process(
    "mc",
    999999,
    label="Monte Carlo",
    color=rgb(55, 126, 184),
    processes=[
        get("dy_lep_10To50"),
        get("dy_lep_50ToInf"),
        get("st"),
        get("tt"),
        get("ttV"),
        get("ttH"),
        get("vv"),
        get("WJetsToLNu"),
    ],
)
od.Process(
    "b",
    999998,
    label="b",
    color=rgb(55, 126, 184),
)
od.Process(
    "c",
    999997,
    label="c",
    color=rgb(60, 179, 113),  # rgb(0, 153, 76), #rgb(50, 180, 140), rgb(102,205,170),
)
od.Process(
    "l",
    999996,
    label="l",
    color=rgb(204, 0, 102),
)
# od.Process(
#     "other",
#     999998,
#     label="Other",
#     color=rgb(55, 126, 184),
#     processes=[
#         #get("ttV"),
#         #get("ttH"),
#     ],
# )

for cfg in [
    config_22,
    config_22EE,
    config_23,
    config_23BPix,
]:  # config_nonEE, config_EE,
    with od.uniqueness_context(cfg.campaign.name):

        cfg.set_aux("stat_model", "model")

        # cfg.set_aux(
        #     "binning",
        #     {
        #         "HF": {
        #             "pt": [20, 30, 50, 70, 100, 140, np.inf],
        #             "abs_eta": [0, 2.5],
        #             "dr": [0, np.inf],
        #         },
        #         "LF": {
        #             "pt": [20, 30, 40, 60, 100, np.inf],
        #             "abs_eta": [0.0, 0.8, 1.6, 2.5],
        #             "dr": [0, np.inf],
        #         },
        #     },
        # )
        cfg.set_aux(
            "binning",
            {
                "HF": {
                    "pt": [20, 40, 60, 80, 120, np.inf],
                    "abs_eta": [0, 2.5],
                    "dr": [0, np.inf],
                },
                "LF": {
                    "pt": [20, 30, 50, 80, np.inf],
                    "abs_eta": [0.0, 0.8, 1.6, 2.5],
                    "dr": [0, np.inf],
                },
            },
        )
        # cfg.set_aux(
        #     "binning",
        #     {
        #         "HF": {
        #             "pt": [20, 50, 80, np.inf],
        #             "abs_eta": [0, 2.5],
        #             "dr": [0, np.inf],
        #         },
        #         "LF": {
        #             "pt": [20, 50, 80, np.inf],
        #             "abs_eta": [0.0, 2.5],
        #             "dr": [0, np.inf],
        #         },
        #     },
        # )

        cfg.set_aux("signal_process", "")

        #
        # process groups
        #
        cfg.set_aux(
            "process_groups",
            {
                "default": [
                    "data",
                    "mc",
                ],
                "plotting": [
                    "data",
                    "b",
                    "c",
                    "l",
                    # "dy",
                    # "st",
                    # "tt",
                    # "vv",
                    # "wjets",
                    # "other",
                ],
                "validation": [
                    "tt",
                    "QCD_pt",
                    # "dy",
                    # "st",
                    # "tt",
                    # "vv",
                    # "wjets",
                    # "other",
                ],
            },
        )

        # flavor IDs for .csv result file
        cfg.set_aux(
            "flavor_ids",
            {
                "LF": 2,
                "c": 1,
                "HF": 0,
            },
        )

        cfg.set_aux(
            "external_shifts",
            [
                "PU",
            ],
        )

        cfg.set_aux(
            "hf_and_lf_shifts",
            [
                "lf",
                "lfstats1",
                "lfstats2",
                "hf",
                "hfstats1",
                "hfstats2",
            ],
        )

        cfg.set_aux(
            "c_shifts",
            [
                "cferr1",
                "cferr2",
            ],
        )

        cfg.set_aux(
            "contamination_factors",
            {
                "lf_Up": 1.2,
                "lf_Down": 0.8,
                "hf_Up": 1.2,
                "hf_Down": 0.8,
            },
        )

        # channels to use
        channels = ["mumu", "ee", "emu", "mu", "e", "had"]
        # remove all channels from cfg which are not in `channels`

        PrepareConfig(
            cfg,
            channels=channels,
            # defines which datasets we will use
            processes=[
                "WJetsToLNu",
                "dy_lep_10To50",
                "dy_lep_50ToInf",
                "st",
                "tt",
                # # "ttV",
                # # "ttH",
                "vv",
                "data_ee",
                "data_mumu",
                "data_e",
                "data_mu",
                "data_emu",
                # full had cross checks
                # "tt_fh",
                # "QCD_pt",
            ],
            ignore_datasets=[],
        )

        from .categories import setup_categories

        setup_categories(cfg)

        from .variables import setup_variables

        setup_variables(cfg)
