def setup_categories(cfg):
    id = 0
    for b_tagger in cfg.aux["working_points"].keys():
        for region in ["HF", "LF"]:
            pt_binning = cfg.aux["binning"][region]["pt"]
            eta_binning = cfg.aux["binning"][region]["abs_eta"]
            dr_binning = cfg.aux["binning"][region]["dr"]
            for pt_min, pt_max in zip(pt_binning[:-1], pt_binning[1:]):
                for eta_min, eta_max in zip(eta_binning[:-1], eta_binning[1:]):
                    for dr_min, dr_max in zip(dr_binning[:-1], dr_binning[1:]):
                        # category_name = f"{region}_{b_tagger}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_dr{dr_min}to{dr_max}"

                        # if region == "LF" and pt_min == 20 and eta_min == 0.0:
                        #     category_name = f"{region}_{b_tagger}_pt{pt_min}to{pt_max}_eta0.0to2.5"
                        #     sf_cat = cfg.add_category(
                        #         category_name,
                        #         id,
                        #         aux={
                        #             "region": region,
                        #             "pt": [pt_min, pt_max],
                        #             "abs_eta": [0.0, 2.5],
                        #             "dr": [dr_min, dr_max],
                        #             "b_tagger": b_tagger,
                        #         },
                        #         tags={"merged", b_tagger},
                        #     )
                        #     id += 1
                        #     for eta_min, eta_max in zip(eta_binning[:-1], eta_binning[1:]):
                        #         for flavour in ["incl", "b", "c", "l"]:
                        #             for probe_jet in range(0, 1 + 1):
                        #                 for channel in ["ch_ee", "ch_mumu", "ch_emu"]:
                        #                     if channel == "ch_emu" and region == "LF":
                        #                         continue
                        #                     # category_name = f"{region}_{channel}_{b_tagger}_probe_jet{probe_jet}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_dr{dr_min}to{dr_max}_{flavour}"
                        #                     category_name = f"{region}_{channel}_{b_tagger}_probe_jet{probe_jet}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_{flavour}"
                        #                     sf_cat.add_category(
                        #                         category_name,
                        #                         id,
                        #                         aux={
                        #                             "flavour": flavour,
                        #                             "channel": channel,
                        #                             "i_probe_jet": probe_jet,
                        #                         },
                        #                     )
                        #                     id += 1
                        # elif region == "LF" and pt_min == 20 and eta_min != 0.0:
                        #     continue
                        # else:
                        category_name = (
                            f"{region}_{b_tagger}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}"
                        )
                        sf_cat = cfg.add_category(
                            category_name,
                            id,
                            aux={
                                "region": region,
                                "pt": [pt_min, pt_max],
                                "abs_eta": [eta_min, eta_max],
                                "dr": [dr_min, dr_max],
                                "b_tagger": b_tagger,
                            },
                            tags={"merged", b_tagger},
                        )
                        id += 1
                        if region is "HF":
                            # category_name = f"c_{b_tagger}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_dr{dr_min}to{dr_max}"
                            category_name = (
                                f"c_{b_tagger}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}"
                            )
                            c_cat = cfg.add_category(
                                category_name,
                                id,
                                aux={
                                    "region": "c",
                                    "pt": [pt_min, pt_max],
                                    "abs_eta": [eta_min, eta_max],
                                    "dr": [dr_min, dr_max],
                                    "b_tagger": b_tagger,
                                },
                                tags={"c", b_tagger},
                            )
                            id += 1
                        for flavour in ["incl", "b", "c", "l"]:
                            for probe_jet in range(0, 1 + 1):
                                for channel in ["ch_ee", "ch_mumu", "ch_emu"]:
                                    if channel == "ch_emu" and region == "LF":
                                        continue
                                    # category_name = f"{region}_{channel}_{b_tagger}_probe_jet{probe_jet}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_dr{dr_min}to{dr_max}_{flavour}"
                                    category_name = f"{region}_{channel}_{b_tagger}_probe_jet{probe_jet}_pt{pt_min}to{pt_max}_eta{eta_min}to{eta_max}_{flavour}"
                                    sf_cat.add_category(
                                        category_name,
                                        id,
                                        aux={
                                            "flavour": flavour,
                                            "channel": channel,
                                            "i_probe_jet": probe_jet,
                                        },
                                    )
                                    id += 1

    for region in ["HF", "LF"]:
        for channel in ["ch_ee", "ch_mumu", "ch_emu"]:
            category_name = f"{region}_{channel}"
            scale_cat = cfg.add_category(
                category_name,
                id,
                aux={"channel": channel, "region": region},
                tags={"scale"},
            )
            id += 1
    cfg.add_category(
        "incl",
        id,
        aux={},
        tags={"scale", "incl"},
    )
