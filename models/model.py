# coding: utf-8

import json
import os
from pathlib import Path

import hist
import numpy as np

import jet_tagging_sf.config.analysis as jtsf_config
from utils.datacard import Datacard, RDict
from utils.util import AsteriskDict, reluncs


class StatModel(Datacard):
    @classmethod
    def requires(cls, task):
        from tasks.group import Rebin

        return Rebin.req(
            task,
            process_group=jtsf_config.cfg.aux["process_groups"]["default"],
            iteration=task.iteration,
            jes_set=task.jes_set,
        )

    @classmethod
    def rebin(cls, variable, h, task):
        from tqdm.auto import tqdm

        if "btag" in variable.name:
            categories = [*h.axes["category"]]
            # new binning files
            if task.recipe == "validation":
                binning_file = "validation.json"
            elif task.recipe == "validationFH":
                binning_file = "validationFH.json"
            elif task.recipe == "selection3":
                from tasks.util import OptimizeBinning

                binning_file = OptimizeBinning.req(task).output().path
            else:
                binning_file = "equal.json"
            with Path(__file__).parent.joinpath(binning_file).open("r") as f:
                data = json.load(f)
                limits = {}
                if task.recipe == "selection3":
                    for pat, bins in data.items():
                        limits[pat] = min(limits.get(pat, len(bins)), len(bins))
                    custom = data  # [str(task.year)]
                else:
                    for dat in data.values():
                        for pat, bins in dat.items():
                            limits[pat] = min(limits.get(pat, len(bins)), len(bins))
                    try:
                        custom = data[str(task.year)]
                    except:
                        print(f"campaign not found in {binning_file}, will use standard binning")
                        custom = {"standard": np.linspace(0.0, 1.0, 21)}
                for pat, bins in custom.items():
                    if pat == "standard":
                        continue
                    if len(bins) > limits[pat]:
                        print(f"trimming {pat} from {len(bins)} to {limits[pat]}: {bins}")
                        custom[pat] = bins[:1] + bins[1 - limits[pat] :]
                custom = AsteriskDict(custom)
                # custom["*"] = list(np.r_[0:1:21j]) # not needed here
            # print(custom)
            custom_redo = False  # turn this on to recalculate (manually) the binning
            if custom_redo:
                for k in custom.keys():
                    custom[k] = None
            hists = {}
            for c in tqdm(categories, unit="category"):
                # print(c)
                if variable.name == "njets":
                    continue
                if "probe_jet" not in c and "selection" in task.recipe:
                    # remove scale categories
                    continue
                hview = h.view()[:, h.axes[1].index(c)].copy()
                hnom = hview[:, h.axes["systematic"].index("nominal"), :]

                bin_centers = h.axes[variable.name].centers
                bin_edges = h.axes[variable.name].edges
                if task.recipe == "selection3":
                    if c.startswith("c_"):
                        continue
                    category = jtsf_config.cfg.get_category(c)
                    parents = list(category.walk_parent_categories())
                    parent_category = parents[0][0]
                    inclusive_parent = parent_category.name  # .split("_pt")[0]
                else:
                    inclusive_parent = c
                # print(inclusive_parent)
                # Custom binning:
                if not custom_redo:  # and inclusive_parent in custom:
                    # print("its acually rebinning")
                    try:
                        new_binning = np.round(custom[inclusive_parent], 3)
                    except:
                        print(
                            f"will use standard binning for {variable.name} and {inclusive_parent}"
                        )
                        # new_binning = custom["standard"]
                        new_binning = np.linspace(0.0, 1.0, 21)
                    # new_binning =  [0.0,0.26,0.34,0.44,0.55,0.66,0.78,0.89,1.0]
                    #

                    if set(new_binning) - set(bin_edges):
                        bad_binning = new_binning
                        new_binning = bin_edges[np.searchsorted(bin_centers, new_binning)]
                        if 1e-5 < np.max(np.abs(bad_binning - new_binning)):
                            print(
                                f"Fixed binning misaligned for {c}:",
                                list(bad_binning),
                                list(new_binning),
                                # sep="\n\t",
                            )

                    # uniquify - no 0-width bins allowed
                    new_binning = np.unique(new_binning)
                    assert new_binning[0] == bin_edges[0], "lower histogram end disconnected"
                    assert new_binning[-1] == bin_edges[-1], "upper histogram end disconnected"
                    hnew = hist.Hist(
                        h.axes["process"],
                        h.axes["systematic"],
                        hist.axis.Variable(
                            new_binning,
                            name=variable.name,
                            label=variable.x_title,
                        ),
                        storage=hist.storage.Weight(),
                    )

                    loc = h.axes[variable.name].index
                    ax = hnew.axes[variable.name]
                    for bin in range(ax.size):
                        # from IPython import embed
                        #
                        # embed()
                        # hnew[..., bin] = hview[
                        #     ...,
                        #     loc(np.round(ax[bin][0], 3)) : loc(np.round(ax[bin][1], 3)),
                        # ].sum(axis=-1)
                        hnew[..., bin] = hview[
                            ...,
                            loc(np.round(ax[bin][0], 3) - 1e-7) : loc(
                                np.round(ax[bin][1], 3) + 1e-7
                            ),
                        ].sum(axis=-1)
                        if ax[bin][0] == 0.0:
                            hnew[..., bin] = hview[
                                ...,
                                loc(0.0) : loc(np.round(ax[bin][1], 3) + 1e-7),
                            ].sum(axis=-1)

                    # hnew[..., 0] = hview[
                    #     ...,
                    #     loc(np.round(ax[0][0], 3)) : loc(np.round(ax[0][1], 3) + 1e-7),
                    # ].sum(axis=-1)
                    # if inclusive_parent == "HF_deepcsv_pt20to30_eta0to2.5":
                    #     print(hnew)
                    #     from IPython import embed
                    #
                    #     embed()
                    #     bug
                    hists[c] = hnew

            return hists

        else:
            print(variable.name)
            """
            no on wants to rebin

            Returns:
                _type_: _description_
            """
            hists = {}
            for c in tqdm(
                h.axes["category"],
                desc=f"rebin - {variable.name}",
                unit="category",
                leave=False,
            ):
                hists[c] = h[:, c, :, :]
            return hists

    # else:
    #     return super(StatModel, cls).rebin(variable, h, task)
